'use strict';

const gulp          = require('gulp');
const babel         = require('gulp-babel');
const sourcemaps    = require('gulp-sourcemaps');
const concat        = require('gulp-concat');
const sass          = require('gulp-sass');
const cleanCSS      = require('gulp-clean-css');
const minifyJS      = require('gulp-minify');
const remove        = require('gulp-clean');
const csso          = require('gulp-csso');
const rename        = require('gulp-rename');
const mmq           = require('gulp-merge-media-queries');
const runSequence   = require('run-sequence');
const pug           = require('gulp-pug');
const imagemin      = require('gulp-imagemin');
const autoprefixer  = require('gulp-autoprefixer');
const browserSync   = require('browser-sync').create();
const wait          = require('gulp-wait');
const purify        = require('gulp-purifycss');

const PATHS = {
  dev: ['_dev/'],
  prod: ['_prod/']
};

//------------------------------ JS Concat Tasks

gulp.task('jsAppDev', () =>
  gulp.src([
    'js/main.js'
  ])
  .pipe(sourcemaps.init())
  .pipe(babel())
  .pipe(concat('app.js'))
  .pipe(sourcemaps.write("."))
  .pipe(gulp.dest(PATHS.dev + 'js/'))
);

gulp.task('jsLibsDev', () =>
  gulp.src([
    'js/feature.js',
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/materialize-css/dist/js/materialize.min.js'
  ])
  .pipe(concat('libs.js'))
  .pipe(gulp.dest(PATHS.dev + 'js/'))
);

//------------------------------

//------------------------------ Minify JS

gulp.task('minify', () =>
  gulp.src(PATHS.dev + 'js/*.js')
  .pipe(minifyJS({
    noSource: true,
    ext: {
      min:'.min.js'
    }
  }))
  .pipe(gulp.dest(PATHS.prod + 'js/'))
);

//------------------------------

//------------------------------ SASS Compile Tasks

gulp.task('sassDev', () =>
  gulp.src('scss/app.scss')
  .pipe(wait(500))
  .pipe(sourcemaps.init())
  .pipe(sass({
    outputStyle: 'expanded'
  }))
  .on('error', sass.logError)
  .pipe(autoprefixer({
    browsers: ['last 1 version'],
    cascade: false
  }))
  .pipe(sourcemaps.write())
  .pipe(rename('assets.dev.css'))
  .pipe(gulp.dest(PATHS.dev + 'css/'))
  .pipe(browserSync.stream())
);

gulp.task('sassProd', () =>
  gulp.src('scss/app.scss')
  .pipe(sass({
    outputStyle: 'compressed'
  }))
  .on('error', sass.logError)
  .pipe(autoprefixer({
    browsers: ['last 2 version', 'ie 10'],
    cascade: false
  }))
  .pipe(rename('assets.min.css'))
  .pipe(mmq())
  .pipe(cleanCSS({
    keepSpecialComments: 0
  }))
  .pipe(purify([PATHS.prod + '**/*.js', PATHS.prod + '/*.html']))
  .pipe(csso({
    report: 'gzip'
  }))
  .pipe(gulp.dest(PATHS.prod + 'css/'))
);

//------------------------------

//------------------------------ Remove Folders

gulp.task('clean', () => gulp.src(['_dev', '_prod'], {read: false}).pipe(remove()));

//------------------------------

//------------------------------ Copy Files Tasks

gulp.task('copyDevFonts', () => gulp.src('node_modules/materialize-css/dist/fonts/**/*').pipe(gulp.dest(PATHS.dev + 'fonts/')));
gulp.task('copyProdFonts', () => gulp.src('node_modules/materialize-css/dist/fonts/**/*').pipe(gulp.dest(PATHS.prod + 'fonts/')));

gulp.task('copyDevFavicons', () => gulp.src('images/favicons/*').pipe(gulp.dest(PATHS.dev + 'images/favicons/')));
gulp.task('copyProdFavicons', () => gulp.src('images/favicons/*').pipe(gulp.dest(PATHS.prod + 'images/favicons/')));

//------------------------------

//------------------------------ ImageMin Tasks

gulp.task('imageMinDev', () =>
  gulp.src([
    'images/**/*.{png,jpg,gif,svg}',
    '!images/favicons/*'
  ])
  .pipe(imagemin())
  .pipe(gulp.dest(PATHS.dev + 'images/'))
);

gulp.task('imageMinProd', () =>
  gulp.src([
    'images/**/*.{png,jpg,gif,svg}',
    '!images/favicons/*'
  ])
  .pipe(imagemin())
  .pipe(gulp.dest(PATHS.prod + 'images/'))
);

//------------------------------

//------------------------------ Pug Compile Tasks

gulp.task('pugDev', () =>
  gulp.src(['pug/*.pug', '!pug/includes/**/*.pug'])
  .pipe(pug({
    pretty: true,
    data: {
      env: 'dev'
    }
  }))
  .pipe(gulp.dest('_dev'))
);

gulp.task('pugProd', () =>
  gulp.src(['pug/*.pug', '!pug/includes/**/*.pug'])
  .pipe(pug({
    pretty: true,
    data: {
      env: 'prod'
    }
  }))
  .pipe(gulp.dest('_prod/'))
);

//------------------------------

//------------------------------ Watch

gulp.task('watch', () => {
  gulp.watch('pug/**/*.pug', () => runSequence('pugDev'));
  gulp.watch('scss/**/*.scss', () => runSequence('sassDev'));
  gulp.watch('js/**/*.js', ['jsAppDev', 'jsLibsDev']);
  gulp.watch('images/**/*.{png,jpg,gif,svg}', ['imageMinDev']);
});

//------------------------------

//------------------------------ BrowserSync

gulp.task('brSync', () => {
  browserSync.init({
    server: {
      watchTask: true,
      baseDir: "_dev/"
    },
    port: 5555,
    open: "external"
  });

  gulp.watch('_dev/*.html').on('change', browserSync.reload);
  gulp.watch('_dev/**/*.js').on('change', browserSync.reload);
  gulp.watch('_dev/images/**/*.{png,jpg,gif,svg}').on('change', browserSync.reload);
});

//------------------------------

//------------------------------ Start Server & Watch

gulp.task('default', ['brSync', 'watch']);

//------------------------------

//------------------------------ Development Build

gulp.task('dev', () => runSequence(
  'clean',
  'sassDev',
  ['jsAppDev','jsLibsDev'],
  'pugDev',
  'imageMinDev',
  ['copyDevFonts','copyDevFavicons']
));

//------------------------------

//------------------------------ Production Build

gulp.task('prod', () => runSequence(
  'clean',
  ['jsAppDev','jsLibsDev'],
  'minify',
  'pugProd',
  'sassProd',
  'imageMinProd',
  ['copyProdFonts','copyProdFavicons']
));

//------------------------------