$(function() {

  // Features Detect Support
  feature.touch ? document.documentElement.classList.add('touch') : document.documentElement.classList.add('no-touch');

  // Mobile Menu
  $(".button-collapse").sideNav();

  // Accordion & Slider
  $('.collapsible').collapsible({
    onOpen(el) {
      const carousetEl = $(el).find('.carousel');

      carousetEl.carousel({
        dist: 0,
        padding: 40,
      });

      $('html').hasClass('no-touch') ? $(el).find('.carousel-nav').fadeIn(100) : $(el).find('.carousel-nav').hide();

      $(el).find('.nav-item_left').on('click', () => {
        carousetEl.carousel('prev');
      });

      $(el).find('.nav-item_right').on('click', () => {
        carousetEl.carousel('next');
      });
    },
    onClose(el) {
      if ($('html').hasClass('no-touch') && $(el).find('.carousel-nav').is(':visible')) {
        $(el).find('.carousel-nav').fadeOut(300);
      }
    }
  });

  // Slider Navigations Arrow


});
